const nums = [5, 10, 1, 9, 2, 8, 3, 7, 4, 6];


const quickSort = (array) => {
	if (array.length < 2) {
		return array;
	}
	else {
		const chosenIndex = array.length-1;
		const chosenEl = array[chosenIndex];
		const a = [];
		const b = [];
		for (let i = 0; i < chosenIndex ; i++) {
			const temp = array[i];
			temp < chosenEl ? a.push(temp) : b.push(temp);
			console.log('a : ', a);
			console.log('b : ', b);
		}
		const output = [...quickSort(a), chosenEl, ...quickSort(b)];
		return output;
	}
}

console.log(nums);
console.log(quickSort(nums));