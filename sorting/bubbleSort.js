const nums = [5, 10, 1, 9, 2, 8, 3, 7, 4, 6];


/************************** bubble sorting **************************/
/*********************** modify initial array ***********************/


/*** compare each element to the next one and swap them if needed ***/
/*********** iterate until swapped variable returns false ***********/

const bubbleSort = (array) => {
	let swapped = false;
	do {
		swapped = false;
		array.forEach((el, i) => {
			if (el > array[i+1]) {
				swapped = true;
				let temp = el;
				array[i] = array[i+1];
				array[i+1] = temp;
			console.log(array);
			}
		});
	} while (swapped);
	return array;
}
console.log(nums);
bubbleSort(nums);
console.log(nums);