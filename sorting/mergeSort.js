const nums = [5, 10, 1, 9, 2, 8, 3, 7, 4, 6];



/************************** merge sorting **************************/
/************************ return new array ************************/



/******** divide : make two arrays out of the original one ********/
/********* sort : compare first values of the small arrays *********/
/********** and push smaller value in a new sorted array **********/
/* add rest of small arrays at the end in case there's still a value left */

const divide = (array) => {
	if (array.length < 2) {
		return array
	}
	else {
		const middle = Math.floor(array.length/2)
		const left = array.slice(0, middle);
		const right = array.slice(middle);
		return mergeSort(divide(left), divide(right));
	}
}

const mergeSort = (left, right) => {
	const sorted = [];
	while(left.length && right.length) {
		if (left[0] <= right[0]) {
			sorted.push(left.shift());
		}
		else {
			sorted.push(right.shift());
		}
	}
	const output = [...sorted, ...left, ... right];
	return output;
}

console.log(nums);
const sorted = divide(nums);
console.log(sorted);