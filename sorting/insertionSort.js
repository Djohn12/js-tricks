const nums = [5, 10, 1, 9, 2, 8, 3, 7, 4, 6];

/************************** insertion sorting **************************/
/************************ modify initial array ************************/

/********************** use two loops to iterate **********************/
/****** second loop stops when reaching element in the first one ******/
/******* move first loop element before second one if needed *******/


const insertionSort = (array) => {
	for (let i = 1; i < array.length ; i++) {
		for (let j = 0; j < i ; j++) {
			if (array[i] < array[j]) {
				let [el] = array.splice(i, 1);
				array.splice(j, 0, el);
			}
		}
	}
	return array;
}
console.log(nums);
insertionSort(nums);
console.log(nums);