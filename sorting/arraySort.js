// sort array of numbers by natural order
// slower on firefox than implemented quicksort and mergesort ?
// cf https://stackoverflow.com/questions/40721767/what-is-the-fastest-way-to-sort-a-largeish-array-of-numbers-in-javascript

array.sort(a, b) {
	return a - b; // a negative result will attest that a < b and swap numbers ?
}
